import falcon
import json
import base64
import getent
import os
import pexpect
import re

class MayfirstAuth:
    parsed_payload = {}
    username = None
    password = None
    public_key = None
    remote_ip = None
    remote_port = None
    home_dir = None
    public_key_matcher = None

    def __init__(self):
        # example: "(ssh-rsa blahblah)( someone@somewhere)"
        self.public_key_matcher = re.compile(r'^((ssh-rsa|ssh-ed25519|ssh-ed25519-sk) [a-zA-z0-9+/=]+)( .*)?$')

    def forbid(self, msg):
        print(msg, flush=True)
        raise falcon.HTTPForbidden

    def parse(self, req):
        parsed = {}
        if req.content_length:
            try:
                payload = json.load(req.stream)
                print(payload)
            except:
                self.forbid("Unparseable JSON {0}".format(req.stream))
            if "username" in payload:
                self.username = payload["username"]
            if "remoteAddress" in payload:
                remote_address = payload["remoteAddress"].split(":")
                try:
                    self.remote_ip = remote_address[0]
                    if len(remote_address) > 1:
                        self.remote_port = remote_address[1]
                except IndexError:
                    self.forbid("Failed to parse IP from {0}".format(self.req_payload))
            if "passwordBase64" in payload:
                try:
                    self.password = base64.b64decode(payload["passwordBase64"])
                except:
                    self.forbid("Failed to base64 decode the password.")
            elif "publicKey" in payload:
                self.public_key = payload["publicKey"]
        else:
            self.forbid("No payload provided.")

    def authorize_username(self):
        if not self.username:
            self.forbid("Username not provided.")
        result = getent.passwd(self.username)
        if not result:
            self.forbid("Failed to find username {0}".format(self.username))
        self.home_dir = dict(result)["dir"]

    def authorize_password(self):
        if not self.password:
            self.forbid("No password provided")
        password = self.password.decode()
        if "'"  in self.username:
            self.forbid("Username contains an apostrophe.")
        try:
            child = pexpect.spawn("su --command 'echo success' '{0}'".format(self.username))
            child.expect("Password:")
            child.sendline(password)
            child.expect("success")
        except pexpect.exceptions.EOF:
            self.forbid("Failed password.")

    def authorize_public_key(self):
        if not self.public_key:
            self.forbid("No public key provided")
        authorized_key_file = "/var/lib/red/keys/{0}/authorized_keys".format(self.username)
        if not os.path.exists(authorized_key_file):
            self.forbid("Authorized key files does not exist ({0})".format(authorized_key_file))
        with open(authorized_key_file) as file:
            if any(self.strip_public_key_line(line) == self.public_key for line in file):
                return
        self.forbid("Public key {0} not in file {1}".format(self.public_key, authorized_key_file))

    def strip_public_key_line(self, line):
        # The line in the authorized_keys file will be something like
        # ssh-rsa blahblahblbase64 someone@somewhere
        # We want to strip out the " someone@somewhere" part and only return
        # the ssh-rsa blablahbase64 part.
        matched = self.public_key_matcher.match(line.rstrip())
        if matched:
            return matched.group(1) 
        print("Invalid authorized keys line: {0}".format(line))
        return ""

    def get_config(self):
        return {
            "config": {
                "backend": "docker",
                "docker": {
                    "connection": {
                        "host": "unix:///var/run/docker.sock"
                     },
                    "execution": {
                        "host": {
                            "binds": [
                                "{0}:{0}".format(self.home_dir)
                            ]
                        },
                        "container": {
                            "user": "1001",
                            "image": "localhost:5000/container-guest"
                        },
                    }
                 }
            }
        }

    def get_directories(self):
        ret = [];
        base_dir = "/home/sites/"
        permissions_directory = "/var/lib/red/site-access"
        d = os.listdir(permissions_directory)
        for name in d:
            path = "{0}/{1}".format(permissions_directory, name)
            if os.path.isfile(path):
                with open(path) as f:
                    if any(line.rstrip() == self.username for line in f):
                        ret.append("{0}/{1}:{0}/{1}".format(base_dir, name))
        return ret

class PasswordResource(MayfirstAuth):
    def on_post(self, req, resp):
        self.req = req
        self.parse(req) 
        self.authorize_username()
        self.authorize_password()
        success = {
            'success': True,
        }
        resp.media = success 

class PubkeyResource(MayfirstAuth):
    def on_post(self, req, resp):
        self.req = req
        self.parse(req) 
        self.authorize_username()
        self.authorize_public_key() 
        success = {
            'success': True,
        }
        resp.media = success 

class ConfigResource(MayfirstAuth):
    def on_post(self, req, resp):
        self.req = req
        self.parse(req) 
        self.authorize_username()
        success = self.get_config() 
        resp.media = success 

app = application = falcon.App()
app.add_route('/password', PasswordResource())
app.add_route('/pubkey', PubkeyResource())
app.add_route('/config', ConfigResource())
