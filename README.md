# May First Container SSH Authentication and Configuration App

[Container SSH](https://github.com/ContainerSSH/ContainerSSH) launches an
docker container for every SSH connection it receives.

It outsources the
[authentication](https://containerssh.io/getting-started/authserver/) of users
and the docker [configuration](https://containerssh.io/reference/configserver/)
of the containers that are launched.

This app provides both. And this git repository explains how to set it up along
with all the other parts to make ContainerSSH work.

## Getting this app working.

### One time setup

This app was setup using python virtual environment:

```
mkdir -p ~/venv
virtualenv ~/venv/containerssh-mayfirst
source ~/venv/containerssh-mayfirst/bin/activate
pip install -r requirements.txt
# Note: can't install getent directly for some reason.
pip install git+https://github.com/tehmaze/getent.git
```

### Launch

```
source ~/venv/containerssh-mayfirst/bin/activate
# Note: the IP address you bind to should be accessible to docker containers
# but ideally not accessible to the outside world.
gunicorn containerssh-mayfirst.app --bind 10.11.13.100 
```

## Getting everything working together

### The ContainerSSH docker container

Once the auth/config server is running, you have to launch the ContainerSSH docker container.

This is the container that manages the show - in other words, when a user tries
to ssh in, they will be first interacting with this container.

You can do that by cd'ing into the `helpers/containerssh` directory and:

 * Copy config.yml.sample to config.yml and edit
 * Copy docker-compose.yml.sample to docker-compose.yml and edit
 * Run: `docker-composer up -d`

### The guest container 

When someone ssh'es into the ContainerSSH docker container, it will
authenticate against our authentication server and it will pull the
configuration of what to do via our authentication server. And, that
configuration will include which Docker image to use as the "guest" container
for the person who logs in.

So, we have to build that guest container.

That happens by cd'ing into `helpers/guest` and running:

  * `create-base-image`
  * `docker build -t guest-container`

### The docker registry

That should be all but... ContainerSSH doesn't work with locally built images.
It has to pull the guest image from a docker registry.

Eek. We don't want to host our images on the public Docker registries, so we
have one more step: we have to setup our own docker registry to run via
localhost. 

We do that by cd'ing into `helpers/registry` and then typing:

    docker-composer up -d

And then we tag our guest container and push it into our new registry with:

    docker image tag container-guest localhost:5000/container-guest
    docker push localhost:5000/container-guest

## Testing

Next, you can test by ssh'ing into it with:

    ssh -o HostKeyAlgorithms=+ssh-rsa -o PubkeyAcceptedAlgorithms=+ssh-rsa -p 2222 test@10.11.13.100

You need to make exceptions for the use of the ssh-rsa protocol until [this bug
is fixed](https://github.com/ContainerSSH/ContainerSSH/issues/535).

